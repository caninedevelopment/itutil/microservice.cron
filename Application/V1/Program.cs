﻿using System.Collections.Generic;

namespace Monosoft.Service.Cron.V1
{
    class Program
    {
        static void Main(string[] args)
        {
            Commands.run runcmd = new Commands.run();
            runcmd.Execute(null); // Start the scheduler

            ITUtil.Common.Console.Program.StartRabbitMq(new List<ITUtil.Common.Command.INamespace>()
            {
                new V1.Commands.Namespace(),
            });
        }
    }
}