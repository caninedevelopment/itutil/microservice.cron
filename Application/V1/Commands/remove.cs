﻿using Monosoft.Service.Cron.V1.DTO;

namespace Monosoft.Service.Cron.V1.Commands
{
    public class remove : ITUtil.Common.Command.DeleteCommand<RemoveTaskRequest>
    {
        public remove() : base("remove a cronjob")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(RemoveTaskRequest input)
        {
            Namespace.logic.RemoveTask(input);
        }
    }
}