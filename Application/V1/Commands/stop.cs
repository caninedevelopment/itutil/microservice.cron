﻿namespace Monosoft.Service.Cron.V1.Commands
{
    public class stop : ITUtil.Common.Command.UpdateCommand<object>
    {
        public stop() : base("stops the scheduler")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(object input)
        {
            if (Namespace.logic.Scheduler != null && Namespace.logic.Scheduler.IsAlive)
            {
                Namespace.logic.EndTaskScheduler();
            }
        }
    }
}
