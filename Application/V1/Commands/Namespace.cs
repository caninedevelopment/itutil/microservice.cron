﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Cron.V1.Commands
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using Monosoft.Service.Cron.V1;

    /// <summary>
    /// CRON Controller.
    /// </summary>
    public class Namespace : ITUtil.Common.Command.BaseNamespace
    {
        internal static readonly Claim IsAdmin = new Claim()
        {
            dataContext = Claim.DataContextEnum.organisationClaims,
            key = "isAdmin",
            description = new LocalizedString[]
            {
                new LocalizedString("en", "User is administrator, i.e. can see all orders"),
            },
        };

        internal static Logic logic = new Logic();

        public Namespace()
            : base ("Cron", new ITUtil.Common.Command.ProgramVersion("1.0.0.1"))
        {
            this.commands.Add(new createScheduled());
            this.commands.Add(new createTimed());
            this.commands.Add(new listAllJobs());
            this.commands.Add(new remove());
            this.commands.Add(new run());
            this.commands.Add(new stop());
            this.commands.Add(new update());
        }
    }
}