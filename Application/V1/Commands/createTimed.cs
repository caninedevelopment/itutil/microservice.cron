﻿using Monosoft.Service.Cron.V1.DTO;

namespace Monosoft.Service.Cron.V1.Commands
{
    public class createTimed : ITUtil.Common.Command.InsertCommand<NewDateTaskRequest>
    {
        public createTimed() : base("Create a task that will run on selected date")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(NewDateTaskRequest input)
        {
            Namespace.logic.AddTask(input);
        }
    }
}