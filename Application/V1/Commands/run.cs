﻿using System.Threading;

namespace Monosoft.Service.Cron.V1.Commands
{
        public class run : ITUtil.Common.Command.UpdateCommand<object>
        {
            public run() : base("starts the scheduler")
            {
                this.Claims.Add(Namespace.IsAdmin);
            }

            public override void Execute(object input)
            {
                if (Namespace.logic.Scheduler == null || !Namespace.logic.Scheduler.IsAlive)
                {
                    Namespace.logic.Scheduler = new Thread(Namespace.logic.TaskScheduler);
                    Namespace.logic.Scheduler.Start();
                }
            }
        }
}
