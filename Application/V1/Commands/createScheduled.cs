﻿using Monosoft.Service.Cron.V1.DTO;

namespace Monosoft.Service.Cron.V1.Commands
{
    public class createScheduled : ITUtil.Common.Command.InsertCommand<NewCronTaskRequest>
    {
        public createScheduled() : base("Create a cron scheduled job, the input is (Minute Hour DayOfMonth Month DayOfWeek). using * means every _ , enter 2/3 will is every third _ starting from 2, enter 2-6 will run every _ between 2 and 6, enter 2,6 to run at 2 _ and 6 _ . An exampel 1 * * 1,7 1-5 will run every day, 1 past every hour, in january and july from monday through friday. More help at \"https://crontab.guru/#*_*_*_*_*\"")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(NewCronTaskRequest input)
        {
            Namespace.logic.AddScheduledTask(input);
        }
    }
}