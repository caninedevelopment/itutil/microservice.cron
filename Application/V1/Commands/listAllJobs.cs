﻿using Monosoft.Service.Cron.V1.DTO;

namespace Monosoft.Service.Cron.V1.Commands
{
    public class listAllJobs : ITUtil.Common.Command.GetCommand<object, TaskList>
    {
        public listAllJobs() : base("List all scheduled jobs")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override TaskList Execute(object input)
        {
            return Namespace.logic.ListAllTasks();
        }
    }
}
