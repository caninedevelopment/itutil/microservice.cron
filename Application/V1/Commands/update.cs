﻿using Monosoft.Service.Cron.V1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.Cron.V1.Commands
{
    public class update : ITUtil.Common.Command.UpdateCommand<TaskInfo>
    {
        public update() : base("remove a cronjob")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(TaskInfo input)
        {
            Namespace.logic.EditTask(input);
        }
    }
}
