﻿// <copyright file="TaskToRun.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Cron.V1.DTO
{
    using System.Collections.Generic;
    using ITUtil.Common.DTO;

    /// <summary>
    /// TaskToRun
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class TaskToRun
    {
        /// <summary>
        /// Gets or sets route
        /// </summary>
        public string route { get; set; }

        /// <summary>
        /// Gets or sets json
        /// </summary>
        public string json { get; set; }

        /// <summary>
        /// Gets or sets claims
        /// </summary>
        public List<MetaData> claims { get; set; }
    }
}
