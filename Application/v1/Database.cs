﻿// <copyright file="Database.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Cron.V1
{
    using System;
    using ITUtil.Common.Utils.Bootstrapper;
    using Monosoft.Service.Cron.V1.DTO;
    using ServiceStack.OrmLite;

    public class Database
    {
        private static OrmLiteConnectionFactory dbFactory = null;
        private static string dbName = "cron";

        public Database(bool autocleanup, SQLSettings settings)
        {
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<TaskInfo>();
            }

            if (autocleanup)
            {
                this.ClearDatabase();
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServerDialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        /// <summary>
        /// CreateTask
        /// </summary>
        /// <param name="input">TaskInfo</param>
        /// <returns>bool</returns>
        public void CreateTask(TaskInfo input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<TaskInfo>(input.id);
                if (dbresult == null)
                {
                    db.Insert<TaskInfo>(input);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementAlreadyExistsException("The CRON job already exist", input.id.ToString());
                }
            }
        }

        /// <summary>
        /// UpdateTask
        /// </summary>
        /// <param name="id">Guid</param>
        /// <param name="updatedInfo">TaskInfo</param>
        /// <returns>bool</returns>
        public void UpdateTask(Guid id, TaskInfo updatedInfo)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (updatedInfo == null)
            {
                throw new ArgumentNullException(nameof(updatedInfo));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<TaskInfo>(id);
                if (dbresult != null)
                {
                    db.Update<TaskInfo>(updatedInfo);
                }
                else
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Task does not exist", id.ToString());
                }
            }
        }

        /// <summary>
        /// GetTaskFromId
        /// </summary>
        /// <param name="id">Guid</param>
        /// <returns>TaskInfo</returns>
        public TaskInfo GetTaskFromId(Guid id)
        {
            using (var db = dbFactory.Open())
            {
                var task = db.SingleById<TaskInfo>(id);
                return task;
            }
        }

        /// <summary>
        /// RemoveTask
        /// </summary>
        /// <param name="id">Guid</param>
        /// <returns>bool</returns>
        public void RemoveTask(Guid id)
        {
            using (var db = dbFactory.Open())
            {
                if (db.SingleById<TaskInfo>(id) == null)
                {
                    throw new ITUtil.Common.Base.ElementDoesNotExistsException("Task does not exist", id.ToString());
                }

                db.Delete<TaskInfo>(x => x.id == id);
            }
        }

        /// <summary>
        /// GetTasksWithUserId
        /// </summary>
        /// <param name="userId">string</param>
        /// <returns>TaskList</returns>
        public TaskList GetTasksWithUserId(string userId)
        {
            TaskList result = new TaskList();
            using (var db = dbFactory.Open())
            {
                result.tasks = db.Select<TaskInfo>(x => x.userId == userId);
                return result;
            }
        }

        /// <summary>
        /// ListAllTarsks
        /// </summary>
        /// <returns>TaskList</returns>
        public TaskList ListAllTasks()
        {
            TaskList result = new TaskList();
            using (var db = dbFactory.Open())
            {
                result.tasks = db.Select<TaskInfo>();
                return result;
            }
        }

        /// <summary>
        /// ClearDatabase
        /// </summary>
        public void ClearDatabase()
        {
            using (var db = dbFactory.Open())
            {
                db.DeleteAll<TaskInfo>();
            }
        }
    }
}