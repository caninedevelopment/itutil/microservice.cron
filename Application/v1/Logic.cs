﻿// <copyright file="Logic.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Cron.V1
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using ITUtil.Common.DTO;
    using ITUtil.Common.RabbitMQ.Request;
    using ITUtil.Common.Utils;
    using Monosoft.Service.Cron.V1.DTO;

    /// <summary>
    /// Logic
    /// </summary>
    public class Logic// : IDisposable
    {
        /// <summary>
        /// Gets or sets Times
        /// </summary>
        private readonly static AutoResetEvent newNextTask = new AutoResetEvent(false);

        private Database db;

        private bool taskSchedulerRunning = false;

        /// <summary>
        /// Constructur for the business logic
        /// </summary>
        /// <param name="emptydb">remove all data from the database</param>
        /// <param name="settingsFilename">which configuration should be loaded (null = default)</param>
        public Logic(bool emptydb = false, string settingsFilename = null)
        {
            var settings = MicroServiceConfig.getConfig(settingsFilename);

            this.db = new Database(emptydb, settings.SQL);
        }

        /// <summary>
        /// Gets or sets nextTask
        /// </summary>
        public TaskInfo NextTask { get; set; } = new TaskInfo();

        /// <summary>
        /// Gets or sets scheduler
        /// </summary>
        public Thread Scheduler { get; set; }

        /// <summary>
        /// CronToDate
        /// </summary>
        /// <param name="cron">string</param>
        /// <returns>DateTime</returns>
        public DateTime CronToDate(string cron)
        {
            if (cron == null)
            {
                throw new ArgumentNullException(nameof(cron));
            }

            List<int> minute = new List<int>();     // Minute splitCron[0] - 0 til 59
            List<int> hour = new List<int>();       // Hour splitCron[1] - 0 til 23
            List<int> monthday = new List<int>();   // MonthDay splitCron[2] - 1 til 31
            List<int> month = new List<int>();      // Month splitCron[3] - 1 til 12
            List<int> weekDay = new List<int>();    // WeekDay splitCron[4] - 0 til 6 hvor 0 er søndag

            // * = hvert tal | ,  flere end en | - Alle mellem | / hvert hvor venstre er start og højre er interval
            string[] splitCron = cron.Split(' ');
            DateTime now = DateTime.Now;
            now.AddMinutes(1);

            for (int i = 0; i < 5; i++)
            {
                List<int> temp = new List<int>();
                int min = 0;
                int max = 0;

                switch (i)
                {
                    case 0:
                        min = 0;
                        max = 59;
                        break;
                    case 1:
                        min = 0;
                        max = 23;
                        break;
                    case 2:
                        min = 1;
                        max = 31;
                        break;
                    case 3:
                        min = 1;
                        max = 12;
                        break;
                    case 4:
                        min = 0;
                        max = 6;
                        break;
                }

                // Finder alle tal for hver del
                string[] komSplit = splitCron[i].Split(',');

                foreach (string s in komSplit)
                {
                    if (s.Contains('/', StringComparison.OrdinalIgnoreCase))
                    {
                        int interval = int.Parse(s.Split('/')[1], ITUtil.Common.Constants.Culture.DefaultCulture);

                        if (!(s.Split('/')[0] == "*"))
                        {
                            min = int.Parse(s.Split('/')[0], ITUtil.Common.Constants.Culture.DefaultCulture);
                        }

                        for (int x = min; min <= x && x <= max; x++)
                        {
                            if ((x - min) % interval == 0)
                            {
                                temp.Add(x);
                            }
                        }
                    }
                    else if (s.Contains('-', StringComparison.OrdinalIgnoreCase))
                    {
                        int smin = int.Parse(s.Split('-')[0], ITUtil.Common.Constants.Culture.DefaultCulture);
                        int smax = int.Parse(s.Split('-')[1], ITUtil.Common.Constants.Culture.DefaultCulture);

                        for (int x = smin; smin <= x && x <= smax; x++)
                        {
                            temp.Add(x);
                        }
                    }
                    else if (s == "*")
                    {
                        for (int x = min; min <= x && x <= max; x++)
                        {
                            temp.Add(x);
                        }
                    }
                    else
                    {
                        temp.Add(int.Parse(s, ITUtil.Common.Constants.Culture.DefaultCulture));
                    }
                }

                // Add til listen den passer i + fjern dublicates
                foreach (int n in temp)
                {
                    switch (i)
                    {
                        case 0:
                            if (!minute.Contains(n))
                            {
                                minute.Add(n);
                            }

                            break;
                        case 1:
                            if (!hour.Contains(n))
                            {
                                hour.Add(n);
                            }

                            break;
                        case 2:
                            if (!monthday.Contains(n))
                            {
                                monthday.Add(n);
                            }

                            break;
                        case 3:
                            if (!month.Contains(n))
                            {
                                month.Add(n);
                            }

                            break;
                        case 4:
                            if (!weekDay.Contains(n))
                            {
                                weekDay.Add(n);
                            }

                            break;
                    }
                }
            }

            minute.Sort();
            hour.Sort();
            monthday.Sort();
            month.Sort();
            weekDay.Sort();

            int startDay = 0;
            int startMonth = this.IndexFromInt(month, DateTime.Now.Month);
            int year = DateTime.Now.Year;

            if (month[startMonth] == DateTime.Now.Month)
            {
                startDay = 0;
            }
            else if (startMonth == 0 && DateTime.Now.Month != 1)
            {
                year++;
            }

            int stopYear = DateTime.Now.Year + 5;

            // Finder den næste DateTimes som passer på cron
            while (year < stopYear)
            {
                for (int m = startMonth; m < month.Count; m++)
                {
                    for (int d = startDay; d < monthday.Count; d++)
                    {
                        for (int h = 0; h < hour.Count; h++)
                        {
                            for (int min = 0; min < minute.Count; min++)
                            {
                                DateTime date = default(DateTime);

                                date = new DateTime(year, month[m], monthday[d], hour[h], minute[min], 0);

                                if (date > now)
                                {
                                    foreach (int i in weekDay)
                                    {
                                        if (date.DayOfWeek == (DayOfWeek)i)
                                        {
                                            return date;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                year++;
                startMonth = 0;
                startDay = 0;
            }

            throw new Exception("Invalid cron input");
        }

        /// <summary>
        /// AddScheduledTask
        /// </summary>
        /// <param name="request">NewCronTaskRequest</param>
        /// <returns>string</returns>
        public void AddScheduledTask(NewCronTaskRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            TaskInfo task = new TaskInfo();
            task.cron = request.cron;
            task.task = request.task;
            task.userId = request.userId;
            task.datetime = this.CronToDate(request.cron);

            this.db.CreateTask(task);

            if (this.NextTask.datetime.Year < 2000 || task.datetime < this.NextTask.datetime)
            {
                Logic.newNextTask.Set();
            }
        }

        /// <summary>
        /// AddTask
        /// </summary>
        /// <param name="request">NewDateTaskRequest</param>
        /// <returns>string</returns>
        public void AddTask(NewDateTaskRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            TaskInfo task = new TaskInfo();

            task.cron = string.Empty;
            task.task = request.task;
            task.userId = request.userId;
            task.datetime = request.datetime;

            this.db.CreateTask(task);

            if (this.NextTask.datetime.Year < 2000 || task.datetime < this.NextTask.datetime)
            {
                Logic.newNextTask.Set();
            }
        }

        /// <summary>
        /// RemoveTask
        /// </summary>
        /// <param name="request">RemoveTaskRequest</param>
        /// <returns>string</returns>
        public void RemoveTask(RemoveTaskRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (this.NextTask.datetime.Year < 2000 || request.taskID == this.NextTask.id)
            {
                Logic.newNextTask.Set();
            }

            this.db.RemoveTask(request.taskID);
        }

        /// <summary>
        /// EditTask
        /// </summary>
        /// <param name="request">TaskInfo</param>
        /// <returns>string</returns>
        public void EditTask(TaskInfo request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            TaskInfo task = db.GetTaskFromId(request.id);

            if (string.IsNullOrEmpty(request.cron))
            {
                task.cron = string.Empty;
                task.datetime = request.datetime;
                if (!string.IsNullOrEmpty(request.task.json) && !string.IsNullOrEmpty(request.task.route))
                {
                    task.task = request.task;
                }
            }
            else
            {
                task.cron = request.cron;
                task.datetime = this.CronToDate(request.cron);
                if (!string.IsNullOrEmpty(request.task.json) && !string.IsNullOrEmpty(request.task.route))
                {
                    task.task = request.task;
                }
            }

            if (this.NextTask.datetime.Year < 2000 || this.NextTask.id == task.id || task.datetime < this.NextTask.datetime)
            {
                Logic.newNextTask.Set();
            }

            db.UpdateTask(request.id, task);
        }

        /// <summary>
        /// ListUserTasks
        /// </summary>
        /// <param name="request">ListTasksRequest</param>
        /// <returns>TaskList</returns>
        public TaskList ListUserTasks(ListTasksRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            TaskList list = this.db.GetTasksWithUserId(request.userId);

            return list;
        }

        /// <summary>
        /// ListAllTasks
        /// </summary>
        /// <returns>TaskList</returns>
        public TaskList ListAllTasks()
        {
            TaskList list = this.db.ListAllTasks();

            return list;
        }

        /// <summary>
        /// TaskScheduler
        /// </summary>
        public void TaskScheduler()
        {
            // Køres i en tråd (eventult lave sleep som køre 24 timer eller når næste task skal køres,
            // efter 24 timer check på om nogen tasks skal køres inden for de næste 24 timer og tilføj dem til liste
            // lave event så den vågner ved ny task, edit task, remove hvis den task ligger inden for tiden)
            this.taskSchedulerRunning = true;

            while (this.taskSchedulerRunning)
            {
                TaskList list = this.db.ListAllTasks();

                Logic.newNextTask.Reset();
                if (this.NextTask.datetime <= DateTime.Now)
                {
                    this.NextTask = new TaskInfo();
                }

                foreach (TaskInfo task in list.tasks)
                {
                    if (task.datetime <= DateTime.Now)
                    {
                        this.RunTask(task.task);

                        if (string.IsNullOrEmpty(task.cron))
                        {
                            this.db.RemoveTask(task.id);
                        }
                        else
                        {
                            task.datetime = this.CronToDate(task.cron);
                            this.db.UpdateTask(task.id, task);
                        }
                    }
                    else
                    {
                        if (this.NextTask.datetime.Year < 2000)
                        {
                            this.NextTask = task;
                        }
                        else if (task.datetime < this.NextTask.datetime)
                        {
                            this.NextTask = task;
                        }
                    }
                }

                TimeSpan idleTime;
                DateTime in24hour = DateTime.Now;
                in24hour = in24hour.AddMilliseconds(-in24hour.Millisecond);
                in24hour = in24hour.AddSeconds(-in24hour.Second);
                in24hour = in24hour.AddDays(1);

                if (!(this.NextTask.datetime.Year < 2000) && this.NextTask.datetime < in24hour)
                {
                    idleTime = this.NextTask.datetime.Subtract(DateTime.Now);
                }
                else
                {
                    idleTime = in24hour.Subtract(DateTime.Now);
                }

                Logic.newNextTask.WaitOne(idleTime);
            }
        }

        /// <summary>
        /// EndTaskScheduler
        /// </summary>
        public void EndTaskScheduler()
        {
            this.taskSchedulerRunning = false;
            Logic.newNextTask.Set();
        }

        /// <summary>
        /// RunTask
        /// </summary>
        /// <param name="task">TaskToRun</param>
        public void RunTask(TaskToRun task)
        {
            if (task == null)
            {
                throw new ArgumentNullException(nameof(task));
            }

            MessageWrapper mw = new ITUtil.Common.DTO.MessageWrapper(
                    new TokenData() { claims = task.claims.ToArray(), tokenId = Guid.Empty, userId = Guid.Empty, validUntil = DateTime.Now.AddDays(1) },
                    DateTime.Now,
                    Guid.NewGuid().ToString(),
                    string.Empty,
                    string.Empty,
                    task.json);

            RequestClient.Instance.FAF(task.route, ITUtil.Common.Utils.MessageDataHelper.ToMessageData(mw));
        }

        private int IndexFromInt(List<int> list, int nr)
        {
            int index = 0;
            foreach (int i in list)
            {
                if (nr <= i)
                {
                    return index;
                }

                index++;
            }

            return 0;
        }
    }
}
