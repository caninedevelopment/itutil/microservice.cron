﻿// <copyright file="DBAndLogicTests.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using Monosoft.Service.Cron.V1;
    using Monosoft.Service.Cron.V1.DTO;
    using NUnit.Framework;

    /// <summary>
    /// DBAndLogicTests
    /// </summary>
    [TestFixture]
    public class DBAndLogicTests
    {
        private static string[] configurations = { "appsettings.mssql.json", "appsettings.postgresql.json" };

        /// <summary>
        /// Claim
        /// </summary>
        private List<MetaData> claim = new List<MetaData> { new MetaData() { key = "rgegr", scope = " ", value = "sf" } };

        /// <summary>
        /// AddCronTask
        /// </summary>
        [Test]
        public void AddCronTask()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewCronTaskRequest request = new NewCronTaskRequest();

                request.cron = "* 1 29 2 *";
                request.task = new TaskToRun();
                request.task.route = "Route";
                request.task.json = "Json";
                request.task.claims = this.claim;
                request.userId = "TestUser";

                logic.AddScheduledTask(request);
            }
        }

        /// <summary>
        /// AddDateTask
        /// </summary>
        [Test]
        public void AddDateTask()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewDateTaskRequest request = new NewDateTaskRequest();

                request.datetime = DateTime.Now;
                request.datetime.AddMonths(1);
                request.task = new TaskToRun();
                request.task.route = "Route";
                request.task.json = "Json";
                request.task.claims = this.claim;
                request.userId = "TestUser";

                logic.AddTask(request);
            }
        }

        /// <summary>
        /// AddDateTask2
        /// </summary>
        [Test]
        public void AddDateTask2()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewDateTaskRequest request = new NewDateTaskRequest();
                request.datetime = DateTime.Now;
                request.datetime.AddMonths(1);
                request.datetime.AddDays(2);
                request.task = new TaskToRun();
                request.task.route = "Route";
                request.task.json = "Json";
                request.task.claims = this.claim;
                request.userId = "TestUser2";

                logic.AddTask(request);
            }
        }

        /// <summary>
        /// GetUsersScheduledTasks
        /// </summary>
        [Test]
        public void GetUsersScheduledTasks()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewDateTaskRequest request = new NewDateTaskRequest();

                request.datetime = DateTime.Now;
                request.datetime.AddMonths(1);
                request.datetime.AddDays(2);
                request.task = new TaskToRun();
                request.task.route = "Route";
                request.task.json = "Json";
                request.task.claims = this.claim;
                request.userId = "TestUser";

                logic.AddTask(request);
                request.task.json = "Json2";
                logic.AddTask(request);

                ListTasksRequest listreq = new ListTasksRequest();
                listreq.userId = request.userId;

                TaskList tasks = logic.ListUserTasks(listreq);
                Assert.AreEqual(2, tasks.tasks.Count , $"Error on {configfile}");
            }
        }

        /// <summary>
        /// GetAllScheduledTasks
        /// </summary>
        [Test]
        public void GetAllScheduledTasks()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);

                NewDateTaskRequest request = new NewDateTaskRequest();

                request.datetime = DateTime.Now;
                request.datetime.AddMonths(1);
                request.datetime.AddDays(2);
                request.task = new TaskToRun();
                request.task.route = "Route";
                request.task.json = "Json";
                request.task.claims = this.claim;
                request.userId = "TestUser";

                logic.AddTask(request);
                request.task.json = "Json2";
                request.userId = "TestUser2";
                logic.AddTask(request);

                TaskList tasks = logic.ListAllTasks();

                Assert.AreEqual(2, tasks.tasks.Count, $"Error on {configfile}");
            }
        }

        /// <summary>
        /// EditTask
        /// </summary>
        [Test]
        public void EditTask()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewDateTaskRequest createRequest = new NewDateTaskRequest();

                createRequest.datetime = DateTime.Now;
                createRequest.datetime.AddMonths(1);
                createRequest.datetime.AddDays(2);
                createRequest.task = new TaskToRun();
                createRequest.task.route = "Route";
                createRequest.task.json = "Json";
                createRequest.task.claims = this.claim;
                createRequest.userId = "TestUser";

                logic.AddTask(createRequest);

                TaskList tasks = logic.ListUserTasks(new ListTasksRequest() { userId = createRequest.userId });

                TaskInfo request = new TaskInfo();
                request.id = tasks.tasks[0].id;
                request.cron = "1 1 29 2 *";
                request.task = new TaskToRun();

                logic.EditTask(request);
            }
        }

        /// <summary>
        /// RemoveTask
        /// </summary>
        [Test]
        public void RemoveTask()
        {
            foreach (var configfile in configurations)
            {
                Logic logic = new Logic(true, configfile);
                NewDateTaskRequest createRequest = new NewDateTaskRequest();

                createRequest.datetime = DateTime.Now;
                createRequest.datetime.AddMonths(1);
                createRequest.datetime.AddDays(2);
                createRequest.task = new TaskToRun();
                createRequest.task.route = "Route";
                createRequest.task.json = "Json";
                createRequest.task.claims = this.claim;
                createRequest.userId = "TestUser";
                logic.AddTask(createRequest);

                TaskList tasks = logic.ListUserTasks(new ListTasksRequest() { userId = createRequest.userId });

                Assert.IsTrue(tasks.tasks.Count > 0);

                RemoveTaskRequest request = new RemoveTaskRequest();
                request.taskID = tasks.tasks[0].id;
                logic.RemoveTask(request);
            }
        }
    }
}