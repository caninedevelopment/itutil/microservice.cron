﻿// <copyright file="SchedulerLogicTests.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using ITUtil.Common.DTO;
    using Monosoft.Service.Cron.V1;
    using Monosoft.Service.Cron.V1.DTO;
    using NUnit.Framework;


    /*
     JIBO NOTE:
     Samtlige test er FUBAR, de tester reelt om "AutoResetEvent" (dotnet) fungere, og ikke om vores egen kode er korrekt.
     Dermed er testene også tvunget ud i at lave thread.sleep, hvilket bryder med best-practise for unittest.
     Testene skal skrives om til at man validere at "next execution date" bliver sat korrekt, når man tilføjer en ny task - ikke om den faktisk bliver kørt.
         */

    ///// <summary>
    ///// SchedulerLogicTests
    ///// </summary>
    //[TestFixture]
    //public class SchedulerLogicTests
    //{
    //    /// <summary>
    //    /// Claim
    //    /// </summary>
    //    private List<MetaData> claim = new List<MetaData> { new MetaData() { key = "rgegr", scope = " ", value = "sf" } };

    //    /// <summary>
    //    /// SchedulerTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerTest()
    //    {
    //        Logic logic = new Logic(true);
    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        NewDateTaskRequest request = new NewDateTaskRequest();
    //        request.datetime = DateTime.Now;
    //        request.datetime = request.datetime.AddMinutes(2);
    //        request.task = new TaskToRun();
    //        request.task.route = "Route";
    //        request.task.json = "test";
    //        request.task.claims = this.claim;
    //        request.userId = "Json";

    //        logic.AddTask(request);

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        Assert.IsTrue(logic.NextTask != null);
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        TaskList tasks = logic.ListAllTasks();

    //        foreach (TaskInfo info in tasks.tasks)
    //        {
    //            if (info.userId == request.userId)
    //            {
    //                t = false;
    //            }
    //        }

    //        logic.EndTaskScheduler();
    //    }

    //    /// <summary>
    //    /// SchedulerRemoveTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerRemoveTest()
    //    {
    //        Logic logic = new Logic(true);
    //        Database db = new Database(true); //why oh why
    //        bool t = true;


    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        // logic.AddTask

    //        // tilføjer 2 tasks som tilføjer en ny task til databasen
    //        // lave så den sletter den ene inden den køre og lave så den anden kommer igennem

    //        // læser om de bægge er kommet i databasen og kun hvis den ene er der paser testen
    //        NewDateTaskRequest request = new NewDateTaskRequest();
    //        request.datetime = DateTime.Now;
    //        request.datetime = request.datetime.AddMinutes(2);
    //        request.task = new TaskToRun();
    //        request.task.route = "Rout";
    //        request.task.json = "test";
    //        request.task.claims = this.claim;
    //        request.userId = "Json";

    //        logic.AddTask(request);

    //        NewDateTaskRequest request2 = new NewDateTaskRequest();
    //        request2.datetime = DateTime.Now;
    //        request2.datetime = request2.datetime.AddMinutes(1);
    //        request2.task = new TaskToRun();
    //        request2.task.route = "Rout";
    //        request2.task.json = "test";
    //        request2.task.claims = this.claim;
    //        request2.userId = "Json2";

    //        logic.AddTask(request2);

    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        ListTasksRequest listtask = new ListTasksRequest();
    //        listtask.userId = request2.userId;
    //        TaskList req2tasks = logic.ListUserTasks(listtask);

    //        RemoveTaskRequest remove = new RemoveTaskRequest();
    //        foreach (TaskInfo task in req2tasks.tasks)
    //        {
    //            if (task.task.json == "Json2")
    //            {
    //                remove.taskID = task.id;
    //            }
    //        }

    //        logic.RemoveTask(remove);

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(40000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        TaskList tasks = db.ListAllTasks();

    //        foreach (TaskInfo info in tasks.tasks)
    //        {
    //            if (info.userId == "NotHear")
    //            {
    //                t = false;
    //            }
    //        }

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Assert.IsTrue(t, string.Empty);
    //    }

    //    /// <summary>
    //    /// SchedulerCronTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerCronTest()
    //    {
    //        Logic logic = new Logic(true);
    //        Database db = new Database(true); //why oh why
    //        bool t = false;

    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        NewCronTaskRequest request = new NewCronTaskRequest();
    //        request.cron = "*/2 * * * *";
    //        request.task = new TaskToRun();
    //        request.task.route = "Rout";
    //        request.task.json = "test";
    //        request.task.claims = this.claim;
    //        request.userId = "Json2";

    //        logic.AddScheduledTask(request);

    //        DateTime date = logic.CronToDate(request.cron);
    //        date = date.AddMinutes(2);

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        TaskList tasks = db.ListAllTasks();

    //        foreach (TaskInfo info in tasks.tasks)
    //        {
    //            if (info.userId == "Json2" && info.datetime == date)
    //            {
    //                t = true;
    //            }
    //        }

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Assert.IsTrue(t, string.Empty);
    //    }

    //    /// <summary>
    //    /// SchedulerEditTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerEditTest()
    //    {
    //        Logic logic = new Logic(true);
    //        Database db = new Database(true); //why oh why
    //        bool t = false;



    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        NewDateTaskRequest request = new NewDateTaskRequest();
    //        request.datetime = DateTime.Now;
    //        request.datetime = request.datetime.AddMinutes(2);
    //        request.datetime = request.datetime.AddSeconds(-request.datetime.Second);
    //        request.task = new TaskToRun();
    //        request.task.route = "Rout";
    //        request.task.json = "test";
    //        request.task.claims = this.claim;
    //        request.userId = "Json2";

    //        logic.AddTask(request);

    //        Thread.Sleep(60000);

    //        ListTasksRequest task = new ListTasksRequest();
    //        TaskInfo taskinfo = new TaskInfo();

    //        task.userId = request.userId;

    //        TaskList list = logic.ListUserTasks(task);

    //        list.tasks[0].datetime = request.datetime.AddHours(1);
    //        logic.EditTask(list.tasks[0]);

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        TaskList tasks = db.ListAllTasks();

    //        foreach (TaskInfo info in tasks.tasks)
    //        {
    //            if (info.datetime == list.tasks[0].datetime)
    //            {
    //                t = true;
    //            }
    //        }

    //        foreach (TaskInfo info in tasks.tasks)
    //        {
    //            if (info.userId == "NotHear")
    //            {
    //                t = false;
    //            }
    //        }

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Assert.IsTrue(t, string.Empty);
    //    }

    //    /// <summary>
    //    /// SchedulerWakeupTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerWakeupTest()
    //    {
    //        Logic logic = new Logic(true);
    //        Database db = new Database(true); //why oh why
    //        bool t = false;
    //        NewDateTaskRequest request = new NewDateTaskRequest();
    //        request.datetime = DateTime.Now;
    //        request.datetime = request.datetime.AddMinutes(10);
    //        request.datetime = request.datetime.AddSeconds(-request.datetime.Second);
    //        request.task = new TaskToRun();
    //        request.task.route = "Rout";
    //        request.task.json = "test";
    //        request.task.claims = this.claim;
    //        request.userId = "Json2";

    //        logic.AddTask(request);

    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        ListTasksRequest task = new ListTasksRequest();
    //        TaskInfo taskinfo = new TaskInfo();

    //        task.userId = request.userId;

    //        TaskList list = logic.ListUserTasks(task);

    //        list.tasks[0].datetime = request.datetime.AddHours(1);
    //        logic.EditTask(list.tasks[0]);

    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        request.task.json = "Json";
    //        request.datetime = request.datetime.AddMinutes(-1);
    //        logic.AddTask(request);

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        foreach (TaskInfo i in list.tasks)
    //        {
    //            if (i.task.json == "Json")
    //            {
    //                RemoveTaskRequest r = new RemoveTaskRequest();
    //                r.taskID = i.id;
    //                logic.RemoveTask(r);
    //            }
    //        }

    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        request.datetime = request.datetime.AddDays(1);

    //        logic.AddTask(request);

    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        Console.WriteLine("Thread running: " + logic.Scheduler.IsAlive);

    //        //if (logic.Times == 3)
    //        {
    //            t = true;
    //        }

    //        Assert.IsTrue(t, string.Empty);
    //    }

    //    /// <summary>
    //    /// SchedulerEmailDateTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerEmailDateTest()
    //    {
    //        Logic logic = new Logic(true);
    //        NewDateTaskRequest request = new NewDateTaskRequest();
    //        request.datetime = DateTime.Now;
    //        request.datetime = request.datetime.AddMinutes(2);
    //        request.datetime = request.datetime.AddSeconds(-request.datetime.Second);
    //        request.task = new TaskToRun();
    //        request.task.route = "emailnotification.v1.Email";
    //        request.task.json = "{\"nameOfSender\":\"CronTest\",\"sender\":\"monx25@hotmail.dk\",\"receivers\":[\"monx25@hotmail.com\"],\"title\":\"CronTestMail\",\"message\":\"Denne mail er sendt som en test af cron\"}";
    //        request.task.claims = new List<MetaData> { new MetaData() { key = "EmailNotification.sendemail", scope = "OrgClaims", value = "true" } };
    //        request.userId = "CronTest";

    //        logic.AddTask(request);

    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(30000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        Console.WriteLine("Thread running: " + logic.Scheduler.IsAlive);
    //        //Console.WriteLine("Wake ups = " + logic.Times);

    //        Assert.IsTrue(true, string.Empty);
    //    }

    //    /// <summary>
    //    /// SchedulerEmailCronTest
    //    /// </summary>
    //    [Test]
    //    public void SchedulerEmailCronTest()
    //    {
    //        Logic logic = new Logic(true);
    //        bool t = false;
    //        NewCronTaskRequest request = new NewCronTaskRequest();
    //        request.cron = "*/3 * * * *";
    //        request.task = new TaskToRun();
    //        request.task.route = "emailnotification.v1.Email";
    //        request.task.json = "{\"nameOfSender\":\"CronTest\",\"sender\":\"monx25@hotmail.dk\",\"receivers\":[\"monx25@hotmail.com\"],\"title\":\"CronTestMail\",\"message\":\"Denne mail er sendt som en test af cron\"}";
    //        request.task.claims = new List<MetaData> { new MetaData() { key = "EmailNotification.sendemail", scope = string.Empty, value = "true" } };
    //        request.userId = "CronTest";

    //        logic.AddScheduledTask(request);

    //        logic.Scheduler = new Thread(logic.TaskScheduler);
    //        logic.Scheduler.Start();

    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(60000);//TODO:: NOT IN MY UNITTESTS!!!!
    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        logic.EndTaskScheduler();

    //        Thread.Sleep(10000);//TODO:: NOT IN MY UNITTESTS!!!!

    //        //Console.WriteLine("Thread running: " + logic.Scheduler.IsAlive);
    //        //Console.WriteLine("Wake ups = " + logic.Times);

    //        t = true;

    //        Assert.IsTrue(t, string.Empty);
    //    }

    //    // Lave en test der køre noget same tid som ovenover så databasen bliver tæstet
    //    // så hvis der tilføjes når den kigger den igenne om der kommer en fejl
    //}
}