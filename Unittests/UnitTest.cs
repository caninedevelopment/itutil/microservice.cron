﻿//// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
//// Copyright 2018 Monosoft Holding ApS
////
//// Licensed under the Apache License, Version 2.0 (the "License");
//// you may not use this file except in compliance with the License.
//// You may obtain a copy of the License at
////
////    http://www.apache.org/licenses/LICENSE-2.0
////
//// Unless required by applicable law or agreed to in writing, software
//// distributed under the License is distributed on an "AS IS" BASIS,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the License for the specific language governing permissions and
//// limitations under the License.
//// </copyright>
// namespace Unittests
// {
//    using System;
//    using Monosoft.Service.Cron.V1;
//    using NUnit.Framework;
//    [TestFixture]
//    public class UnitTest
//    {
//        public Logic Logic = new Logic();
//        public string Cron = "* * 12 8 *"; // 0-59 * * * * //
//        public DateTime TestDate = new DateTime(2017, 1, 1, 1, 1, 0);
//        // CronToDateTest er en anden metode end den programmet bruger
//        #region Speed old vs new
//        [Test]
//        public void CronToDate()
//        {
//            bool t = true;
//            DateTime date = this.Logic.CronToDate(this.Cron);
//            Assert.IsTrue(t, "Passed With no error");
//        }
//        [Test]
//        public void CronToDatev2()
//        {
//            bool t = true;
//            DateTime date = this.Logic.CronToDate(this.Cron);
//            Assert.IsTrue(t, "Passed With no error");
//        }
//        #endregion
//        #region InvalidInput
//        [Test]
//        public void CronInvalidDate()
//        {
//            bool t = false;
//            try
//            {
//                DateTime date = this.Logic.CronToDateTest("0 0 31 9 *", new DateTime(2019, 8, 13, 1, 1, 1));
//            }
//            catch (Exception e)
//            {
//                if (e.Message == "Invalid cron input")
//                {
//                    t = true;
//                }
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        #region 1 af dem
//        [Test]
//        public void CronMinTest()
//        {
//            bool t = false;
//            string testCron = "1 * * * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 1, 2, 1, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Faild");
//        }
//        [Test]
//        public void CronHourTest()
//        {
//            bool t = false;
//            string testCron = "* 0 * * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 2, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Faild");
//        }
//        [Test]
//        public void CronDayTest()
//        {
//            bool t = false;
//            string testCron = "* * 13 * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 13, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Faild");
//        }
//        [Test]
//        public void CronMonthTest()
//        {
//            bool t = false;
//            string testCron = "* * * 5 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 5, 1, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Faild");
//        }
//        [Test]
//        public void CronWeekDayTest()
//        {
//            bool t = false;
//            string testCron = "* * * * 5";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 6, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Faild");
//        }
//        #endregion
//        #region 2 af dem
//        [Test]
//        public void CronMinHourTest()
//        {
//            bool t = false;
//            string testCron = "57 0 * * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 2, 0, 57, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinDayTest()
//        {
//            bool t = false;
//            string testCron = "40 * 20 * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 20, 0, 40, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinMonthTest()
//        {
//            bool t = false;
//            string testCron = "26 * * 5 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 5, 1, 0, 26, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "26 * * * 3";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 4, 0, 26, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourDayTest()
//        {
//            bool t = false;
//            string testCron = "* 20 18 * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 18, 20, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourMonthTest()
//        {
//            bool t = false;
//            string testCron = "* 10 * 12 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 12, 1, 10, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* 13 * * 4";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 5, 13, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronDayMonthTest()
//        {
//            bool t = false;
//            string testCron = "* * 13 6 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 6, 13, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronDayWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* * 17 * 1";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 4, 17, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* * * 10 3";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 10, 4, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        #region 3 af dem
//        [Test]
//        public void CronMinHourDayTest()
//        {
//            bool t = false;
//            string testCron = "27 2 26 * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 26, 2, 27, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinHourMonthTest()
//        {
//            bool t = false;
//            string testCron = "44 2 * 4 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 4, 1, 2, 44, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinHourWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "17 1 * * 5";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 6, 1, 17, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinDayMonthTest()
//        {
//            bool t = false;
//            string testCron = "32 * 14 2 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 2, 14, 0, 32, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinDayWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "42 * 10 * 3";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 5, 10, 0, 42, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "12 * * 6 1";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 6, 5, 0, 12, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourDayMonthTest()
//        {
//            bool t = false;
//            string testCron = "* 20 12 7 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 7, 12, 20, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourDayWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* 3 10 * 2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 10, 3, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* 12 * 2 4";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 2, 2, 12, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronDayMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "* * 3 2 1";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2020, 2, 3, 0, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        #region 4 af dem
//        [Test]
//        public void CronMinHourDayMonthTest()
//        {
//            bool t = false;
//            string testCron = "12 3 9 2 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 2, 9, 3, 12, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinHourDayWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "22 1 3 * 2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 3, 1, 22, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinHourMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "33 22 * 11 4";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 11, 2, 22, 33, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMinDayMonthWeekdayTest()
//        {
//            bool t = false;
//            string testCron = "43 * 18 12 2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2018, 12, 18, 0, 43, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHourDayMonthTestWeekday()
//        {
//            bool t = false;
//            string testCron = "* 20 18 12 2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2018, 12, 18, 20, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        #region alle 5
//        [Test]
//        public void CronAlleTest()
//        {
//            bool t = false;
//            string testCron = "2 2 2 2 2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2021, 2, 2, 2, 2, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronBlankTest()
//        {
//            bool t = false;
//            string testCron = "* * * * *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 1, 1, 2, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        #region , - /
//        [Test]
//        public void CronRaekkeTest()
//        {
//            bool t = false;
//            string testCron = "20-40 10-15 13-26 4-6 1-3";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 4, 17, 10, 20, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMultiTest()
//        {
//            bool t = false;
//            string testCron = "43,20 10,23 6,25 3,2 1,4";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 2, 6, 10, 20, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMultiRaekkeTest()
//        {
//            bool t = false;
//            string testCron = "43-57,10-22 2-5,10-20 6-15,12-18 1-2,11-12 5-6,0-1";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 6, 2, 10, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronHverTest()
//        {
//            bool t = false;
//            string testCron = "2/5 2/5 2/5 1/3 */2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 1, 7, 2, 2, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        [Test]
//        public void CronMultihverTest()
//        {
//            bool t = false;
//            string testCron = "1/8,2/3 5/2,2/5 1/10,12/4 3/4,2/5 1/3,2/2";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2017, 2, 11, 2, 1, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//        #endregion
//        [Test]
//        public void CronSkudaarTest()
//        {
//            bool t = false;
//            string testCron = "* 1 29 2 *";
//            DateTime date = this.Logic.CronToDateTest(testCron, this.TestDate);
//            DateTime result = new DateTime(2020, 2, 29, 1, 0, 0);
//            if (date == result)
//            {
//                t = true;
//            }
//            Assert.IsTrue(t, "Catched exception - Invalid cron input");
//        }
//    }
// }